# Scripts-tools

Scripts for Flitter\XFCE desktop.

### xfce4-screenshoter-wrapper 

Make it easy taking screenshots with xfce-screenshoter:

- xfce4-screenshoter-wrapper -r -- select the region
- xfce4-screenshoter-wrapper -w -- active window
- xfce4-screenshoter-wrapper -f -- fullscreen

Screenshot will be saved in $FOLDER you select.

### xfce4-centerwin

Center current active window, xfce for some reason, can't do it native.

### xfce4-compositor 

Toggle compositor on the fly.

### pulse-mute-microphone

Toggle pulseaudio microphone mute.
